//
//  main.m
//
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal;
    @try {
        retVal = UIApplicationMain(argc, argv, nil, @"AppController");
    }
    @catch (NSException *exception) {
        NSLog(@"%@", [exception callStackSymbols]); //< ★1
        @throw exception; //< ★2
    }
    @finally {
        [pool release];
    }
    return retVal;
}

