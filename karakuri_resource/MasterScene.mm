#import "MasterScene.h"
#import "ScenesManager.h"


@implementation MasterScene
+ (CCScene *)sceneWithSceneId:(NSInteger)sceneId nextBool:(BOOL)nextPage {
    CCScene *scene = [MasterScene node];
    CCLayer *layer = [FirstLayer layerWithSceneId:sceneId];
    
    [scene addChild:layer];
    return scene;
}
//@synthesize menuObj;
@end

#pragma mark -


//FirstLayerクラス
@implementation FirstLayer
#pragma mark インスタンスの初期化/解放

// インスタンスの初期化を行うメソッド。
// このシーン（レイヤー）で使用するオブジェクトを作成・初期化します。
+ (id)layerWithSceneId:(NSInteger)sceneId{
    return [[[self alloc] initWithSceneId:sceneId] autorelease];
}

-(id)initWithSceneId:(NSInteger)sceneId{
    self = [super init];
    if(self){
        self.tag = sceneId;
        sceneId2 = sceneId;
        self.isTouchEnabled = YES;
        
        [MenuObject Pagenum:sceneId2];
        
        //画面サイズ
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        
        if(sceneId2 != 0){
            self.menuObj = [MenuObject node];
            self.menuObj.position = ccp(-winSize.width/2+40,-winSize.height/2+40);
            [self addChild:self.menuObj z:100];
        }
        
        
        
        //背景の設置
        NSString *backgroundPath = [[NSString alloc] init];
        if(sceneId2 == 0){
            backgroundPath = [NSString stringWithFormat:@"index.pvr.gz"];
        }else if(sceneId2 == 3){
            backgroundPath = [NSString stringWithFormat:@"p2.pvr.gz"];
        }else if(sceneId2 == 2){
            backgroundPath = [NSString stringWithFormat:@"credit.pvr.gz"];
        }else{
            backgroundPath = [NSString stringWithFormat:@"p%d.pvr.gz",sceneId2];
        }
       
        CCTexture2D *background = [[CCTextureCache sharedTextureCache] addImage:backgroundPath];
        spriteBack = [CCSprite spriteWithTexture:background rect:CGRectMake(0,0,winSize.width,winSize.height)];
        spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);
        [self addChild: spriteBack];

        if(sceneId2 == 0){
            self.indexPage = [indexPage node];
            self.indexPage.position = ccp(winSize.width/2,winSize.height/2);
            [self addChild:self.indexPage];
        }
 
        if(sceneId2 == 1){
            self.p1 = [p1 node];
            self.p1.position = ccp(winSize.width/2,winSize.height/2);
            [self addChild:self.p1];
        }
        
        //Creditページ
        if(sceneId2 == 2){
            self.credit = [credit node];
            self.credit.position = ccp(winSize.width/2,winSize.height/2);
            [self addChild:self.credit];
        }
        
        //ゆかり後ページ
        if(sceneId2 == 3){
            self.yukari = [yukari node];
            self.yukari.position = ccp(winSize.width/2,winSize.height/2);
            [self addChild:self.yukari];
        }
        
    }
    return self;

}
-(void) onExitTransitionDidStart{
    [self FadeOut];

}
-(void) onEnterTransitionDidFinish{
    //ナレーション
    [super onEnterTransitionDidFinish];
    [self narration];
}

-(void) onExit{
    [super onExit];
}

#pragma mark ナレーション
-(void)narration{
    NSUserDefaults *userDefault =[NSUserDefaults standardUserDefaults];
    int setting_narrar = [userDefault integerForKey:@"SETTING2"];
    
    if(!setting_narrar){
        NSString *narrationMp3 = [[NSString alloc] initWithFormat:@"narration%d.mp3",sceneId2 ];
        playnarra = [[CDAudioManager sharedManager] audioSourceForChannel:kASC_Left];

        [playnarra load:narrationMp3];
        [playnarra setNumberOfLoops:0];
        playnarra.volume = 1.0;
        playnarra.delegate = self;
        [playnarra play];
        [narrationMp3 release];

    }
}
-(void)cdAudioSourceDidFinishPlaying:(CDLongAudioSource *)audioSource{
    if(sceneId2 > 0 ){
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        CCSprite *tugihezButton = [CCSprite spriteWithSpriteFrameName:@"tugihe.png"];
        tugihezButton.position = ccp(winSize.width/2+175,40);
        tugihezButton.scale = 0.0f;
        [self addChild:tugihezButton z:14];
        
        id scale =[CCScaleTo actionWithDuration:0.5f scale:1.0];
        id ease = [CCEaseBounceOut actionWithAction:scale];
        [tugihezButton runAction:ease];
    }
    
}




#pragma mark タッチイベント
-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch  = [touches anyObject];
    CGPoint location = [touch locationInView:[touch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];


}

#pragma mark イベント処理

-(void)FadeOut{
    CDLongAudioSource *player2 = [[CDAudioManager sharedManager] audioSourceForChannel:kASC_Left];
    CDLongAudioSourceFader* fader = [[CDLongAudioSourceFader alloc] init:player2 interpolationType:kIT_Exponential startVal:player2.volume endVal:0.0];
    [fader setStopTargetWhenComplete:YES];
    //Create a property modifier action to wrap the fader
    CDXPropertyModifierAction* action = [CDXPropertyModifierAction actionWithDuration:1.0 modifier:fader];
    [fader release];//Action will retain
    id func = [CCCallBlock actionWithBlock:^{
    }];
    [[CCDirector sharedDirector].actionManager  addAction:[CCSequence actions:action,func, nil] target:player2 paused:NO];
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
    
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
	[super dealloc];
}
@end
