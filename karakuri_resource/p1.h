//
//  p1.h
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SoundEffect.h"

@interface p1 : CCLayer {
    CCSprite *hasami2;
    CCSprite *fude;
    CCSprite *hon;
    CCSprite *hiyoko;
    
    CCSprite *ya;
    CCSprite *niku;
    CCSprite *alley;
    CCSprite *taimatu;
    int p1_touchNo1;
    int p1_touchNo2;
    
    CCSprite *osuno;
    CCSprite *susanoo;
    CCSprite *osuno2;
    CCSprite *susanoo2;
}

@end
