
#import "ScenesManager.h"
#include "MasterScene.h"


static struct {
    int current;
    int next;
    NSString *sceneClassName;
} Graph[] = {
    { 0,  1, @"MasterScene"},
    { 1,  2, @"MasterScene"},
    { 2,  3, @"MasterScene"},
    { 3,  4, @"MasterScene"},
};

@implementation SceneManager
+ (CCScene *)nextSceneOfScene:(CCNode *)from choice:(NSInteger)choise nextBool:(BOOL)nextPage {
    int nextSceneId;
    
    if (choise == 0) {
        nextSceneId = Graph[from.tag].next;

    }else if (choise == 1){
        nextSceneId = Graph[from.tag].current-1;
    }else if (choise == 2){
        nextSceneId = 0; //タイトルへ戻る
    }else if (choise == 3){
        nextSceneId = 2; //クレジット
    }else{
        nextSceneId = 3; //ゆかり後
    }
    CCScene *nextScene;
    if(nextSceneId == 4){
        nextSceneId = 0;
        nextScene = [NSClassFromString(Graph[nextSceneId].sceneClassName)sceneWithSceneId:nextSceneId nextBool:nextPage];
    }else{
        nextScene = [NSClassFromString(Graph[nextSceneId].sceneClassName)sceneWithSceneId:nextSceneId nextBool:nextPage];
    }
    nextScene.tag = nextSceneId;
    return nextScene;
}

@end
