//
//  SoundEffect.h
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SimpleAudioEngine.h"

@interface SoundEffect : NSObject {
    ALuint se;
}
+(void)seLoad;
+(void)sePlay:(NSString *)soundName;
@end
