//
//  SoundEffect.m
//
//

#import "SoundEffect.h"

@implementation SoundEffect

+(void)seLoad{
    //サウンドのプリロード
    //[self seLoading:@"ofn_se_p1_hiyoko1.caf"];
    
}

+(void)seLoading:(NSString *)soundName{
    [[SimpleAudioEngine sharedEngine] preloadEffect:soundName];
}

+(void)sePlay:(NSString *)soundName{
    NSUserDefaults *userDefault =[NSUserDefaults standardUserDefaults];
    int setting_effect = [userDefault integerForKey:@"SETTING3"];
    if (!setting_effect ) {
        [[SimpleAudioEngine sharedEngine] playEffect:soundName];
    }
    
}



@end
