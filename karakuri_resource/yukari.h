//
//  yukari.h
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import <MapKit/MapKit.h>


@interface yukari : CCLayer <UIWebViewDelegate,MKMapViewDelegate>{
    UIWebView *webView_;
    UIToolbar *toolBar;
    UIActivityIndicatorView *activityInducator_;
    MKMapView *mv;
    CLLocationCoordinate2D destination;
    CCLayer *layer_sanageyama;
    CCLayer *layer_jinjya;
    CCLayer *layer_maturi;
    CCLayer *layer_yahagi;
    CCLayer *layer_shirahige;
    
}

@end
