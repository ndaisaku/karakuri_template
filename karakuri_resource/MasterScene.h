#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CCTransitionHelper.h"
#import "SimpleAudioEngine.h"
#import "MenuObject.h"
#import "SoundEffect.h"

#import "indexPage.h"
#import "p1.h"
#import "credit.h"
#import "yukari.h"

#import "CDXPropertyModifierAction.h"



@interface MasterScene : CCScene {
}
+ (CCScene *)sceneWithSceneId:(NSInteger)sceneId nextBool:(BOOL)nextPage;

@end

@interface FirstLayer : CCLayer<AVAudioPlayerDelegate,CDLongAudioSourceDelegate>{
    bool start;
    int optionnum3[3];
    CCSprite *spriteBack;
    int sceneId2;
    CDLongAudioSource *playnarra;
}

@property (nonatomic, retain)MenuObject *menuObj;
@property (nonatomic, retain)indexPage *indexPage;
@property (nonatomic, retain)p1 *p1;
@property (nonatomic, retain)credit *credit;
@property (nonatomic, retain)yukari *yukari;

// 初期化処理を行った上でインスタンスを返すメソッド
+ (id)layerWithSceneId:(NSInteger)sceneId;
- (id) initWithSceneId:(NSInteger)sceneId;

@end


