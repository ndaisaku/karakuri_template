//
//  MenuObject.h
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CCTransitionHelper.h"

@interface MenuObject : CCNode {
    CCSprite *spriteBack;
    bool helpHidden;
    bool mojiHidden;
    
    CCSprite *helpBg;
    CCSprite *textImage;
    CCSprite *textBg;
    
    CCSprite *spriteImage[10];
    CCSprite *alertBackground;
    CCSprite *alertBox;
    
    
    bool p5_count_left;
    bool p5_count_right;
    bool p5_count_top;
    bool alert_hajime;
}

+(void)Pagenum:(int)pagenum;


@end
